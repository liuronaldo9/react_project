import React, { Fragment }from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { createGlobalStyle } from "styled-components"

const GlobalStyle = createGlobalStyle`
  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed, 
  figure, figcaption, footer, header, hgroup, 
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }
  article, aside, details, figcaption, figure, 
  footer, header, hgroup, menu, nav, section {
    display: block;
  }
  body {
    line-height: 1;
  }
  ol, ul {
    list-style: none;
  }
  blockquote, q {
    quotes: none;
  }
  blockquote:before, blockquote:after,
  q:before, q:after {
    content: '';
    content: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  };
  @font-face {font-family: "iconfont";
  src: url('./statics/iconfont/iconfont.eot?t=1554499711378'); /* IE9 */
  src: url('./statics/iconfont/iconfont.eot?t=1554499711378#iefix') format('embedded-opentype'), /* IE6-IE8 */
  url('data:application/x-font-woff2;charset=utf-8;base64,d09GMgABAAAAAASQAAsAAAAACKAAAARDAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHEIGVgCDBgqFCIQoATYCJAMMCwgABCAFhG0HOBtwB1FUTSJkf4JsyAbpe5GiRJ8xu7uYBzgl5CUNzoAghXvh68USfEPxM+oEG6zIvgeACKpl69nZj6TgQIUoUSmUR1EoJApHeTQqRKEwGmGROFw24iau+QH6gtGg/+JRludN7UkV+jzebFogw22tZR4AVdw7lEVWqHBnL0eQY9XKDuhpNVC+1gxKMsunZHmrNs3OH+sUpVOyaJKmClmzrghFCjfX53eqWwN8ezdvEJG8fbABZ5B4rCkMuk6OIMRIQqlllIdt9YSy/udwaWNpFs4PlMvcg3oBxgEFNMb0KNECCRCP/Yaxi8u4TqBvFgo8unl6hbpKmxeIC0MTUI9p1BJi6MrbOVuruFXRLdflDty4349/+6hTtJK29Pj8WoGzr6HnMr3582EjcPszgdlFYg2gEme5llMlP7IG1pck5fL0tiqUvvxNg8uRRW39Hx5FEG2aZBBhl8Rlp7gmJ5QGeXvNPTlt22nE3kFMqdpJ2spQ7+hFR0yc7YzUybEtDBLW+vDwmXSbI4uySSJgNcIHV5yLRu5DW1jGccYJJABHT6UfKiMlZdYGBN6qgB7EXTOQ4AqBmlGSYQ+ZjlB7bByU6vowGQxwBcEhfPqRnZVzQArCogTE4DjhFaaOdag+Ew72Nh0hlEGwydYeyfRtHmwhUOqqjmi8Ko9tBNjUMyDp3mR5HgnMIqUrYtUJKwhPIUb/2Zbuid0c2fRhYhcsjxU9GsdKJdHtN0SYNBF7qcmkGxykw6TFVGwwneSgIGYdRq1i+CSzrs2LFRWp0EdS/69xGi7AAwVfm0aXTntT1EoHlQ6OkaJpkWIODrE/bJNSx437+n96CnrExHumEOoELopVAGgGRW8zf8x2XGlgRwoR4ECMp3udcN0u4F0u7YI2IzZ1+PB55psWh2PA30l8Jx3EyIymEHl4sOZeUrScKM6TNlJaWrs53sRS8fS97pJaWBpaUiFsmBc99WUZBEWX2AlLDYzT36EzDU0rTRQPsZ6QRbKj0ZX/y19n2ixLXsMlrb3RVD/P5uw/vUJhYnU2oSF+ABqp12oCNOVgcj/Db7ys797RdV3Bx36vXHejIur7dJU7I/XquTU7qszXtFlFjslRrek+4lKALFbqhRfCuHd72xQgNIUuy4VQdMxA6ppDVuwatAxYh7auXehbJeweMMFWErWHFTGAMOqEYtgbpFEPZMW+oGXaD9pGowR9JzE6csBCGM1LJhGRgmKOhvRc1dBHTS//mbTYFhzlu+R34tDwI7XeJuO35BL3scJJtL0bqaiy5yAPbiPb9tBkzyQZrfUo8g+IjZr3orXkOdB0iBEhQhRIlEMGyeNS7d6RZurzz4gmZhO4pK3E+I6wkDE4ouDIGshbiVur7VxGOxKaPVdExbNUzONAPDQRW494kJk/yUSkyJreIuQ7QKBual3JenyZc337oE87N6NERtW+JMFSr+RxLlwAAA==') format('woff2'),
  url('./statics/iconfont/iconfont.woff?t=1554499711378') format('woff'),
  url('./statics/iconfont/iconfont.ttf?t=1554499711378') format('truetype'), /* chrome, firefox, opera, Safari, Android, iOS 4.2+ */
  url('./statics/iconfont/iconfont.svg?t=1554499711378#iconfont') format('svg'); /* iOS 4.1- */
}

.iconfont {
  font-family: "iconfont" !important;
  font-size: 16px;
  font-style: normal;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
`


ReactDOM.render(
  <Fragment>
    <App />
    <GlobalStyle/>
  </Fragment>,
  document.getElementById('root'));

