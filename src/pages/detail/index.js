import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  DetailWrapper,
  Header,
  Content,
  BackTop
} from './style'
import { actionCreators } from './store/'

class Detail extends PureComponent {
  scrollTop () {
    let timer = setInterval(() => {
      let scrollTop = document.documentElement.scrollTop
      let ispeed = Math.floor(-scrollTop / 9)
      document.documentElement.scrollTop = document.body.scrollTop = scrollTop + ispeed
      if (scrollTop === 0) {
        clearInterval(timer)
      }
    }, 19)
  }
  componentWillMount(){
    document.getElementById('root').scrollIntoView(true)
  }
  compoentWillUnMount() {
    window.removeEventListener('scroll', this.props.chnageTopShow)
  }
  bindEvents() {
    window.addEventListener('scroll', this.props.chnageTopShow)
  }

  render() {
    return (
      <DetailWrapper>
        <Header>
          {this.props.title}
        </Header>
        <Content>
          <img src={this.props.imgUrl} alt="" />
          <div>
            {this.props.text.split("\n").map((i, key) => {
              return <p key={key}>{i}</p>;
            })}
          </div>
          {
            this.props.showTop ?  
            <BackTop onClick={this.scrollTop}>
            <img className="top" src="https://static.thenounproject.com/png/40632-200.png" alt=""/>
          </BackTop>  : null
          }
        </Content>
      </DetailWrapper>
    )
  }
  componentDidMount() {
    this.bindEvents()
    this.props.getDetail(this.props.match.params.id)
  }
}

const mapState = (state) => ({
  title: state.getIn(['detail', 'title']),
  imgUrl: state.getIn(['detail', 'imgUrl']),
  text: state.getIn(['detail', 'text']),
  showTop: state.getIn(['detail', 'showTop'])
})

const mapDispatch = (dispatch) => ({
  getDetail(id) {
    dispatch(actionCreators.getDeatilData(id))
  },
  chnageTopShow() {
    if(document.documentElement.scrollTop > 90) {
      dispatch(actionCreators.backTopShow(true))
    } else {
      dispatch(actionCreators.backTopShow(false))
    }
  }
})

export default connect(mapState, mapDispatch)(Detail)

