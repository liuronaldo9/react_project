import styled from 'styled-components'

export const DetailWrapper = styled.div`
  width: 620px
  margin: 0 auto
  overflow: hidden
  padding-bottom: 100px
`
export const Header = styled.div`
  margin: 50px 0 20px 0
  font-size: 34px
  line-height: 44px
  color: #333333
  font-weight: bold
`

export const Content = styled.div`
  color: #2f2f2f
  img {
    width: 100%
    height: 400px
  }
  p {
    margin: 25px 0;
    font-size: 16px;
    line-height: 30px;
  }
`
export const BackTop = styled.div`
  position: fixed
  right: 100px
  bottom: 100px
  width: 60px
  height: 60px
  line-height: 60px
  text-algin: center
  cursor: pointer
  .top {
    height: 60px
    width: 60px
  }
  .toptext {
    height: 60px
    width: 60px
  }
`