import { fromJS } from 'immutable'
import * as constants from './actionTypes'

const defaultState = fromJS({
  title: '',
  imgUrl: '',
  text: '',
  showTop: false
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_DETAIL_DATA:
      // console.log(action.data)
      return state.merge({
        title: fromJS(action.data.title),
        imgUrl: fromJS(action.data.imgUrl),
        text: fromJS(action.data.text)
      })
    case constants.BACK_TO_TOP:
      return state.set('showTop', action.show)
    default:
      return state
  }
}


