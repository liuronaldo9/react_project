import * as constants from './actionTypes'
import axios from 'axios'

const getDeatilList = (data) => {
  return {
    type: constants.GET_DETAIL_DATA,
    data
  }
}

export const getDeatilData = (id) => {
  return (dispatch) => {
    axios.get('https://9psmeazsv0.execute-api.ap-southeast-2.amazonaws.com/dev/react-list/' + id).then(res => {
      // console.log(res.data)
      const data  = res.data
      dispatch(getDeatilList(data))
    })
  }
}
export const backTopShow = (show) => {
  return {
    type: constants.BACK_TO_TOP,
    show
  }
}