import React, { Component } from 'react'
import {
  AboutWrapper
} from './style'
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';

const photos = [
  { src: 'https://trk.localvox.com/sites/default/files/styles/480x240/public/content_images/learn-to-surf-hawaii.jpg?itok=iK81E3dE', width: 4, height: 2 },
  { src: 'https://instagram.fakl8-1.fna.fbcdn.net/vp/084971a7bbd1945bd682d4d8dba8ffbe/5D368655/t51.2885-15/e35/c0.62.500.500/21980246_1895378050791720_7603447532584173568_n.jpg?_nc_ht=instagram.fakl8-1.fna.fbcdn.net', width: 1, height: 1 },
  { src: 'https://www.booksurfcamps.com/static/files/images/ip/io/hh/kl/content.jpg', width: 4, height: 2 },
  { src: 'https://www.backpackerguide.nz/wp-content/uploads/2016/08/The-Best-Surf-Regions-in-New-Zealand.jpg', width: 3, height: 4 },
  { src: 'https://image.redbull.com/rbcom/010/2016-08-31/1331815085727_1/0100/0/1/the-image-of-russell-b-that-stunned-the-surf-world.jpg', width: 4, height: 3 },
  { src: 'https://i.pinimg.com/originals/3b/54/34/3b543496e0e41bd8d451e9810243a28c.jpg', width: 3, height: 4 },
  { src: 'https://thehandsomecollective.com/wp-content/uploads/2016/10/IMG_9842-by-fran-miller.jpg', width: 4, height: 3 },
  { src: 'https://cdn.concreteplayground.com/content/uploads/2015/12/Surfing-Sydney.jpeg?webp=false', width: 4, height: 3 },
  { src: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=480128477,3621179564&fm=200&gp=0.jpg', width: 4, height: 3 }
];


export default class About extends Component {
  constructor() {
    super();
    this.state = { currentImage: 0 };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
  }
  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  render() {
    return (
      <AboutWrapper>
        <Gallery photos={photos} onClick={this.openLightbox} />
        <Lightbox images={photos}
          onClose={this.closeLightbox}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          currentImage={this.state.currentImage}
          isOpen={this.state.lightboxIsOpen}
        />
      </AboutWrapper>
    )
  }
}
