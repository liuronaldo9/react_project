import React, { PureComponent } from 'react'
import Recommend from './components/Recommend';
import List from './components/List';
import { connect } from 'react-redux'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { actionCreators } from './store'

import {
  HomeWrapper,
  HomeLeft,
  HomeRight,
  BackTop
} from './style.js'
class Home extends PureComponent {
  scrollTop () {
    let timer = setInterval(() => {
      let scrollTop = document.documentElement.scrollTop
      let ispeed = Math.floor(-scrollTop / 9)
      document.documentElement.scrollTop = document.body.scrollTop = scrollTop + ispeed
      if (scrollTop === 0) {
        clearInterval(timer)
      }
    }, 19)
  }
  componentDidMount() {
    this.props.changeHomeInfo()
    this.bindEvents()
  }

  compoentWillUnMount() {
    window.removeEventListener('scroll', this.props.chnageTopShow)
  }
  bindEvents() {
    window.addEventListener('scroll', this.props.chnageTopShow)
  }

  render() {
    return (
      <div>
        <HomeWrapper>
          <HomeLeft>
          <Carousel infiniteLoop autoPlay swipeable={false} showThumbs={false}>
            <div>
            <img src="https://raglansurfingschool.co.nz/wp-content/uploads/2014/07/campervan2.jpg" alt="" style={{"width": "625px", "height": "270px"}}/>
            </div>
            <div>
            <img src="https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/unnamed.jpg" alt="" style={{"width": "625px", "height": "270px"}}/>
            </div>
            <div>
            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1554881465156&di=6d534982d2f5817b37e78dcebf0c2e22&imgtype=0&src=http%3A%2F%2Fi3.3conline.com%2Fimages%2Fpiclib%2F201103%2F14%2Fbatch%2F1%2F88039%2F1300057912504j7c98zb6hb.jpg" alt="" style={{"width": "625px", "height": "270px"}}/>
            </div>
            <div>
            <img src="https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1266062662,3122352983&fm=26&gp=0.jpg" alt="" style={{"width": "625px", "height": "270px"}}/>
            </div>
            <div>
            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1554881489682&di=20f305e8ff843df7ff31b8e8dd2c1534&imgtype=0&src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2017-10-23%2F59ed5f0dcb1ac.jpg" alt="" style={{"width": "625px", "height": "270px"}}/>
            </div>
            </Carousel>
            <div className="line"></div>
            <List />
          </HomeLeft>
          <HomeRight>
            <Recommend />
          </HomeRight>
          {
            this.props.showTop ?  <BackTop onClick={this.scrollTop}>
            <img className="top" src="https://static.thenounproject.com/png/40632-200.png" alt=""/>
          </BackTop>  : null
          }
        </HomeWrapper>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  showTop: state.getIn(['home', 'showTop'])
})

const mapDispatchToProps = (dispatch) => ({
  changeHomeInfo() {
    dispatch(actionCreators.getHomeListData())
  },
  chnageTopShow() {
    if(document.documentElement.scrollTop > 90) {
      dispatch(actionCreators.backTopShow(true))
    } else {
      dispatch(actionCreators.backTopShow(false))
    }
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)



