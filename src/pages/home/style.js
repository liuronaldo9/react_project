import styled from 'styled-components'

export const HomeWrapper = styled.div`
  width: 960px
  margin: 0 auto
  overflow: hidden
`
export const HomeLeft = styled.div`
  width: 625px
  margin-left: 15px
  padding-top: 30px
  float: left
  .line {
    border-bottom: 1px solid #dcdcdc;
  }
`
export const HomeRight = styled.div`
  width: 280px
  float: right
`
export const ListItem =styled.div`
	padding: 20px 0
	border-bottom: 1px solid #dcdcdc
	overflow: hidden
	.list-pic {
		display: block
		width: 150px
		height: 100px
    float: right
    border-radius: 10px
	}
`
export const ListInfo =styled.div`
  width: 475px
  float: left
  .title {
    font-size: 18px
    line-height: 27px
    font-weight: bold
    color: #333
  }
  .desc {
    font-size: 13px
    line-height: 24px
    color: #999
  }
`
export const RecommendWraaper =styled.div`
  margin: 30px 0
  width: 280px
`
export const RecommendItem =styled.div`
  width: 280px
  height: 50px
  margin-bottom: 30px
  background: url(${(props) => props.imgUrl}) no-repeat;
  background-size: contain
`
export const LoadMore = styled.div`
  width: 100%
  height: 40px
  margin: 30px 0
  line-height: 40px
  text-align: center
  background: #a5a5a5
  border-radius: 20px
  color: #fff
  cursor: pointer
`
export const BackTop = styled.div`
  position: fixed
  right: 100px
  bottom: 100px
  width: 60px
  height: 60px
  line-height: 60px
  text-algin: center
  cursor: pointer
  .top {
    height: 60px
    width: 60px
  }
  .toptext {
    height: 60px
    width: 60px
  }
`