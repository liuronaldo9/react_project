import axios from 'axios'
import { fromJS } from 'immutable'
import * as constants from './actionTypes'


const changeHomeData = function (result) {
  return {
    type: constants.CHANGE_HOME_DATA,
    result: result
  }
}

const addHomeData = function (list, nextPage) {
  return {
    type: constants.ADD_HOME_DATA,
    list: fromJS(list),
    nextPage
  }
}

export const getHomeListData = () => {
  return (dispatch) => {
    // console.log(dispatch)
    axios.get('https://9psmeazsv0.execute-api.ap-southeast-2.amazonaws.com/dev/react-list').then(res => {
      const result = res.data
      dispatch(changeHomeData(result))
    })
  }
}

export const getMoreList = (page) => {
  return (dispatch) => {
    axios.get('/api/homeList.json?page=' + page).then(res => {
      const result = res.data.data
      // console.log(result)
      dispatch(addHomeData(result, page + 1))
    })
  }
}

export const backTopShow = (show) => {
  return {
    type: constants.BACK_TO_TOP,
    show
  }
}

