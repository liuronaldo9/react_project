import { fromJS } from 'immutable'
import * as constants from './actionTypes'

const defaultState = fromJS({
  articleList: [],
  recommendList: [{
    "id": 1,
    "imgUrl": "https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/quiksilver_logo.jpg"
  },
  {
    "id": 2,
    "imgUrl": "https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/roxy_logo.png"
  },
  {
    "id": 3,
    "imgUrl": "https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/Billabong_logo.jpg"
  },
  {
    "id": 4,
    "imgUrl": "https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/Oneill_logo.png"
  },
  {
    "id": 5,
    "imgUrl": "https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/Vans_logo.png"
  }],
  articlePage: 1,
  showTop: false
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.CHANGE_HOME_DATA:
      return state.merge({
        articleList: fromJS(action.result)
      })
    case constants.ADD_HOME_DATA:
      return state.merge({
        'articleList': state.get('articleList').concat(action.list),
        articlePage: action.nextPage
      })
    case constants.BACK_TO_TOP:
      return state.set('showTop', action.show)
    default:
      return state
  }
}


