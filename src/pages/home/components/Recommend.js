import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { RecommendWraaper, RecommendItem } from '../style'

class Recommend extends PureComponent {
  render() {
    return (
      <RecommendWraaper>
        {
          this.props.recommendList.map(item => {
            return (
              <RecommendItem
                key={item.get('id')}
                imgUrl={item.get('imgUrl')}
              />
            )
          })
        }
      </RecommendWraaper>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    recommendList: state.getIn(['home', 'recommendList']),
  }
}

export default connect(mapStateToProps, null)(Recommend)