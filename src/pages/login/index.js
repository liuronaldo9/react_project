import React, { PureComponent } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  LoginWrapper,
  LoginBox,
  Input,
  Button
}
  from './style.js'
  import { actionCreators } from './store/'

class Login extends PureComponent {
  render() {
    if(!this.props.loginStatus) {
      return (
        <LoginWrapper>
          <video autoPlay loop muted id="myVideo">
            <source src="https://s3-ap-southeast-2.amazonaws.com/familys.images/property-images/surfbackground.mp4" type="video/mp4" />
          </video>
          <LoginBox>
            <div className="title">LOGIN</div>
            <Input placeholder="Account" ref={(input) => {this.account = input}} />
            <Input placeholder="Password" type="password" ref={(input) => {this.password = input}}/>
            <Button
              onClick={() => this.props.login(this.account.value, this.password.value)}
            >Login</Button>
          </LoginBox>
        </LoginWrapper>
      )
    } else {
      return <Redirect to='/home'/>
    }    
  }
}

const mapState = (state) => ({
  loginStatus: state.getIn(['login', 'login'])
})

const mapDispatch = (dispatch) => ({
  login(accountElm, passwordElm) {
    dispatch(actionCreators.login(accountElm, passwordElm))
  }
})

export default connect(mapState, mapDispatch)(Login)