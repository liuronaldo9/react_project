import styled from 'styled-components'

export const LoginWrapper = styled.div`
  z-index: 0
  #myVideo {
    width: 100% 
    height: 100%
  }
`
export const LoginBox = styled.div`
  position: absolute
  width: 400px
  height: 190px
  margin-top: -500px;
  margin-left: 450px;
  padding-top: 20px
  background: #fff
  opacity: 0.8
  border-radius: 10px
  box-shadow: 0 0 8px rgba(0,0,0,.1);
  .title {
    padding: 10px;
    font-weight: 400;
    font-weight: 700;
    text-align: center
    color: #ea6f5a;
    border-bottom: 1px solid #777;
  }
`
export const Input = styled.input`
  display: block
  width: 200px
  height: 30px
  line-height: 30px
  padding: 0 10px
  margin: 10px auto
  color: #777
`
export const Button = styled.div`
  width: 220px
  height: 30px
  line-height: 30px
  color: #fff
  background: #3194d0
  border-radius: 15px
  margin: 10px auto
  text-align: center
`