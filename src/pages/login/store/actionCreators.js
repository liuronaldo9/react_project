import * as constants from './actionTypes'
// import axios from 'axios'


const handleLogin = () => ({
  type: constants.LOGIN_IN_SUCCESS
})

export const login = (account, password) => {
  return (dispatch) => {
    // axios.post('/').then(res => {
    //   const result =res.data.data
    // })
    dispatch(handleLogin())
  }
}

export const logout = () => ({
  type: constants.LOGOUT_SUCCESS
})