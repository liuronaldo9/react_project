import { fromJS } from 'immutable'
import * as constants from './actionTypes'

const defaultState = fromJS({
  login: true
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.LOGIN_IN_SUCCESS:
      return state.set('login', true)
    case constants.LOGOUT_SUCCESS:
    return state.set('login', false)
    default:
      return state
  }
}


