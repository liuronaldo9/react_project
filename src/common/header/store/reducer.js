import * as constants from './actionTypes'
import { fromJS } from 'immutable'

const defaultState = fromJS({
  focused: false,
  mouseIn: false,
  list: [],
  page: 1,
  totalPage: 1
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.SEARCH_FOCUS:
      return state.set('focused', true)
    case constants.SEARCH_BULR:
      return state.set('focused', false)
    case constants.CHANGE_MOUSE_ENTER:
      return state.set('mouseIn', true)
    case constants.CHANGE_MOUSE_LEAVE:
      return state.set('mouseIn', false)
    case constants.CHANGE_PAGE:
      return state.set('page', action.page)
    case constants.CHANGE_INFO_LIST:
      return state.merge({
        list: action.data,
        totalPage: action.totalPage
      })
    default: 
      return state
  }
}


