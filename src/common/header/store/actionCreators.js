import * as constants from './actionTypes'
import { fromJS } from 'immutable'
import axios from 'axios'

export const searchBlur = function() {
  return {
    type: constants.SEARCH_BULR
  }
}

export const searchFocus = function() {
  return {
    type: constants.SEARCH_FOCUS
  }
}

export const mouseLeave = function() {
  return {
    type: constants.CHANGE_MOUSE_LEAVE
  }
}

export const mouseEnter = function() {
  return {
    type: constants.CHANGE_MOUSE_ENTER
  }
}

export const changePage = function(page) {
  return {
    type: constants.CHANGE_PAGE,
    page
  }
}

const changeInfoList = function(data) {
  return {
    type: constants.CHANGE_INFO_LIST,
    data: fromJS(data),
    totalPage: Math.ceil(data.length / 2 )
  }
}

export const getList = () => {
  return (dispatch) => {
    axios.get('/api/handleList.json').then(res => {
      const data = res.data.data
      dispatch(changeInfoList(data))
    }).catch(err => {
      console.log('error')
    })
  }
}