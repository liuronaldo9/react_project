import styled from 'styled-components'
import logoPic from '../../statics/logo.jpg'

export const HeaderWrapper = styled.div`
	position: relative
	height: 56px;
	border-bottom: 1px solid #f0f0f0;
`
export const Logo = styled.div`
	position: absolute;
	top:0;
	left:0;
	display: block;
	width: 100px;
	height: 56px;
	background: url(${logoPic});
	background-size: contain;
	background-repeat:no-repeat;
`
export const Nav = styled.div`
	width: 960px;
	height: 100%;
	margin: 0 auto;
`
export const NavItem = styled.div`
	line-height: 56px;
	padding: 0 15px;
	font-size: 17px;
	cursor: pointer;
	color: #333
  &.left {
		float: left;
	}
	&.right {
		float: right;
		color: #969696
	}
	&.active {
		color: #ea6f5a;
	}
`
export const NavSearch = styled.input.attrs({
	placeholder: 'Search'
})`
	&.enter {
		transition: all .5s ease-out
	}
	&.enter-active {
		width: 240px
	}
	&.enter-done {
		width: 240px
	}
	&.exit {
		transition: all .5s ease-out
	}
	&.exit-active {
		width: 160px
	}
	&.exit-done {
		width: 160px
	}
	width: 160px;
	height: 38px;
	padding: 0 35px 0 30px
	margin-left: 20px
	font-size: 14px
	box-sizing: border-box
	border: none;
	margin-top: 9px;
	outline: none;
	border-radius: 19px;
	background: #eee;
	color: #666
	&::placeholder {
		color: #999
	}
`
export const SearchWrapper = styled.div`
	position: relative
	float: left;
	.iconfont {
		position: absolute;
		right: 5px
		width: 30px
		line-height: 30px
		border-radius: 15px
		bottom: 5px
		text-align: center
		&.focused {
			background: #777
			color: white
		}
	}
`
export const SearchInfo = styled.div`
	position: absolute;
	left: 0
	top: 56px
	z-index: 1
	width: 240px;
	padding: 0 20px;
	box-shadow: 0 0 8px rgba(0, 0, 0, .2);
	background: #fff
`
export const SearchInfoTiele = styled.div`
	margin-top: 20px;
	margin-bottom: 15px;
	line-height: 20px
	font-size: 14px
	color: #969696
`
export const SearchInfoSwitch =styled.span`
	float: right
	font-size: 13px
	cursor: pointer;
`
export const SearchInfoItem =styled.a`
	display: block
	margin-right: 10px
	margin-bottom: 15px
	float: left
	padding: 0 5px
	font-size: 12px
	line-height: 20px
	border: 1px solid #ddd
	color: #787878
	border-radius: 2px
	cursor: pointer;
`
export const SearchInfoList =styled.div`
	overflow: hidden
`



