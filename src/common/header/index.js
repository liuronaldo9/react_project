import React, { PureComponent } from 'react'
import { CSSTransition } from 'react-transition-group'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { actionCreators } from './store'
import { actionCreators as loginActionCreators } from '../../pages/login/store'
import {
  HeaderWrapper,
  Logo,
  Nav,
  NavItem,
  NavSearch,
  SearchWrapper,
  SearchInfo,
  SearchInfoItem,
  SearchInfoTiele,
  SearchInfoSwitch,
  SearchInfoList
} from './style.js'

class Header extends PureComponent {

  openSite(i) {
    switch (i) {
      case 0:
        window.open('https://www.metservice.com/marine/surf/region-auckland-east-coast')
        break;
      case 1:
        window.open('https://www.google.com/search?q=surfboard&npsic=0&rflfq=1&rlha=0&rllag=-36838598,174696614,7828&tbm=lcl&ved=2ahUKEwjvoZH0s8DhAhUOeH0KHXdMCPYQjGp6BAgKEEM&tbs=lrf:!2m4!1e17!4m2!17m1!1e2!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10&rldoc=1#rlfi=hd:;si:;mv:!1m2!1d-36.565528199999996!2d174.793953!2m2!1d-36.97817!2d174.4082494;tbs:lrf:!2m1!1e2!2m1!1e3!2m4!1e17!4m2!17m1!1e2!3sIAE,lf:1,lf_ui:10')
        break;
      case 2:
        window.open('https://www.google.com/search?q=surfing&source=lnms&tbm=isch&sa=X&ved=0ahUKEwinxqKktMDhAhXQTX0KHQaEBjkQ_AUIDigB&biw=1280&bih=652')
        break;
      case 3:
        window.open('http://www.surf-forecast.com')
        break;
      case 4:
        window.open('https://www.google.com/search?q=surf+lesson&npsic=0&rflfq=1&rlha=0&rllag=-36619646,174571535,32087&tbm=lcl&ved=2ahUKEwiSudjGtMDhAhUbeH0KHUigAzgQjGp6BAgJEEQ&tbs=lrf:!2m4!1e17!4m2!17m1!1e2!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10&rldoc=1#rlfi=hd:;si:;mv:!1m2!1d-36.0703673!2d176.02791670000002!2m2!1d-37.9304012!2d174.33840239999998;tbs:lrf:!2m1!1e2!2m1!1e3!2m4!1e17!4m2!17m1!1e2!3sIAE,lf:1,lf_ui:10')
        break;
      case 5:
        window.open('https://www.google.com/search?q=surfing&source=lnms&tbm=vid&sa=X&ved=0ahUKEwiEyODQtMDhAhUjjuYKHV-qCaAQ_AUIECgD&biw=1280&bih=652')
        break;
      default:
        break;
    }
  }
  showSearchInfo() {
    const pageList = []
    const totalPage = this.props.totalPage
    const jsList = this.props.list.toJS()
    for (let i = (this.props.page - 1) * 2; i < this.props.page * 2; i++) {
      pageList.push(
        <SearchInfoItem key={i}
          onClick={() => this.openSite(i)}
        >
          {jsList[i]}
        </SearchInfoItem>
      )
    }
    if (this.props.focused || this.props.mouseIn) {
      return (
        <SearchInfo onMouseEnter={this.props.handleMouseIn}
          onMouseLeave={this.props.handleMouseLeave}
        >
          <SearchInfoTiele>
            Top Search
            <SearchInfoSwitch onClick={() => this.props.handlePageChange(this.props.page, totalPage)}>
              Next
            </SearchInfoSwitch>
          </SearchInfoTiele>
          <SearchInfoList>
            {pageList}
          </SearchInfoList>
        </SearchInfo>
      )
    } else {
      return null
    }
  }
  render() {
    if(!this.props.login) {
      return null
    } else {
      return (
        <div>
          <HeaderWrapper>
            <Link to='/home'>
              <Logo />
            </Link>
            <Nav>
              <Link to='/home'>
                <NavItem className='left'>HOME</NavItem>
              </Link>
              <Link to='/photo'>
                <NavItem className='left'>PHOTO</NavItem>
              </Link>
                <Link to='/'>
                  <NavItem className='right' onClick={this.props.logout}>LOGOUT</NavItem>
                </Link> 
              <NavItem className='right'>
                <i className="iconfont">&#xe60d;</i>
              </NavItem>
              <SearchWrapper>
                <CSSTransition
                  in={this.props.focused}
                  timeout={500}
                >
                  <NavSearch
                    onBlur={this.props.handleBlur}
                    onFocus={this.props.handleFocused}
                  ></NavSearch>
                </CSSTransition>
                <i className={this.props.focused ? 'focused iconfont' : 'iconfont'}>
                  &#xe6df;
                </i>
                {this.showSearchInfo()}
              </SearchWrapper>
            </Nav>
          </HeaderWrapper>
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    focused: state.getIn(['header', 'focused']),
    list: state.getIn(['header', 'list']),
    page: state.getIn(['header', 'page']),
    totalPage: state.getIn(['header', 'totalPage']),
    mouseIn: state.getIn(['header', 'mouseIn']),
    login: state.getIn(['login', 'login'])
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    handleFocused() {
      dispatch(actionCreators.getList())
      dispatch(actionCreators.searchFocus())
    },
    handleBlur() {
      dispatch(actionCreators.searchBlur())
    },
    handleMouseIn() {
      dispatch(actionCreators.mouseEnter())
    },
    handleMouseLeave() {
      dispatch(actionCreators.mouseLeave())
    },
    handlePageChange(page, totalPage) {
      if (page < totalPage) {
        dispatch(actionCreators.changePage(page + 1))
      } else {
        dispatch(actionCreators.changePage(1))
      }
    },
    logout() {
      dispatch(loginActionCreators.logout())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)


