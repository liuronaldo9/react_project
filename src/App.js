import React, { Component } from 'react';
import Header from './common/header'
import { BrowserRouter, Route } from 'react-router-dom'
import store from './store'
import { Provider } from 'react-redux'
import Home from './pages/home/'
import About from './pages/about/'
import Detail from './pages/detail/'
import Login from './pages/login'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Header />
          <Route
            path='/home' exact component={Home}
          ></Route>
          <Route
            path='/' exact component={Login}
          ></Route>
          <Route
            path='/detail/:id' exact component={Detail}
          ></Route>
          <Route
            path='/photo' exact component={About}
          ></Route>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
